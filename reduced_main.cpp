//opencv
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>
//C++
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
using namespace cv;
using namespace std;

// Global variables
Mat frame; //current frame
Mat fgMaskMOG2; //fg mask fg mask generated by MOG2 method
Ptr<BackgroundSubtractor> pMOG2; //MOG2 Background subtractor

std::vector<Rect> selections;

void subtractInRectangle(char* videoFilename, char* outputFilename);
void orderComponents(std::vector<int> &biggestComponents, std::vector< std::vector<Point> > &components, Mat &component_labels, Mat &component_stats, int num_components);

int main(int argc, char* argv[])
{
    if(argc < 7)
	{
		//argv[1] must be the input video file path, argv[2] must be the output file path, argv[3] - argv[6] are needed for a minimum of 1 selection
		std::cout << "Invalid ammount of arguments" << std::endl;
		exit(EXIT_FAILURE);
	}
	
	for(int i=3; i<argc; i+=4){
		int x= strtol(argv[i], NULL, 10);
		int y= strtol(argv[i+1], NULL, 10);
		int w= strtol(argv[i+2], NULL, 10);
		int h= strtol(argv[i+3], NULL, 10);

		Rect rect = Rect(Point(x, y), Point(x + w, y+ h));
		selections.push_back(rect);
	}
	subtractInRectangle(argv[1], argv[2]);

    //destroy GUI windows
    destroyAllWindows();
    return EXIT_SUCCESS;
}

void subtractInRectangle(char* videoFilename, char* outputFilename)
{
    VideoCapture capture(videoFilename);
    if(!capture.isOpened()){
        //error in opening the video input
        cerr << "Unable to open video file: " << videoFilename << endl;
        exit(EXIT_FAILURE);
    }

    //output file management
    ofstream output_file;
    output_file.open(outputFilename);
    if(!output_file.is_open())
    {
        //error in opening the txt output
		cerr << "Unable to open output file: centroides.csv" << endl;
        exit(EXIT_FAILURE);
    }
    output_file << "frame,selection,x,y" << std::endl;

    while( capture.read(frame) ){
        //read the current frame
        Mat gray_frame;
        cvtColor(frame, gray_frame, CV_RGB2GRAY);

        Mat binary_frame;
        threshold(gray_frame, binary_frame, 80, 255, THRESH_BINARY_INV);

        int last_index = selections.size();
        for(int selection_index=0; selection_index<last_index; selection_index++)
        {
            Mat rect;
            rect.create(selections[selection_index].height, selections[selection_index].width, CV_8UC1);

            for(int i=0; i<selections[selection_index].height; i++)
            {
            for(int j=0; j<selections[selection_index].width; j++)
            {
                rect.at<unsigned char>(i,j) = binary_frame.at<unsigned char>(selections[selection_index].y + i, 
                                                                             selections[selection_index].x + j);
            }
            }

            Mat component_labels;
            Mat component_stats;
            Mat component_centroids;

            int num_components = connectedComponentsWithStats(             rect, component_labels, 
                                                                component_stats, component_centroids);

            std::vector< std::vector<Point> > components;
            std::vector<int> biggestComponents;

            orderComponents(biggestComponents, components, component_labels, 
                              component_stats, num_components);

			//(note) index has to be 1 because 0 is the background
			int centroid_x = trunc(component_centroids.at<double>(biggestComponents[1], 0));
			int centroid_y = trunc(component_centroids.at<double>(biggestComponents[1], 1));

			if(centroid_x < 0){ centroid_x = 0; }
			//else if(centroid_x > selections[selection_index].width){ centroid_x = selections[selection_index].width; }

			if(centroid_y < 0){ centroid_y = 0; }
			//else if(centroid_y > selections[selection_index].height){ centroid_y = selections[selection_index].height; }

			Vec2d centroid = Vec2d( centroid_x + selections[selection_index].x, 
									centroid_y + selections[selection_index].y );
			
			//(note) each line of the file will be structured as:
			// 'frame index','selection label','centroid.x','centroid.y'
			output_file << capture.get(CAP_PROP_POS_FRAMES) << "," << selection_index << "," << centroid[0] << "," << centroid[1] << std::endl;
        }
    }

    output_file.close();
    capture.release();
}

void orderComponents(std::vector<int> &biggestComponents, std::vector< std::vector<Point> > &components, Mat &component_labels, Mat &component_stats, int num_components)
{
    //Given a label Mat "component_labels" and a stat Mat "component_stats" (generated by connectedComponentWithStats),
    // fills a adjacency list of Points in "components" and a vector of component indexes 
    // (ordered by area increasingly) in "biggestComponents"
    components.clear();
    bool component_in_vector[num_components];

    for(int i=0; i<num_components; i++)
    {
        components.push_back(std::vector<Point>());
        component_in_vector[i] = false;
    }

    for(int i=0; i<component_labels.cols; i++)
    {
    for(int j=0; j<component_labels.rows; j++)
    {
        int component_index = component_labels.at<int>(Point(i,j));
        components[ component_index ].push_back( Point(i,j) );

        int component_area = component_stats.at<int>(component_index, CC_STAT_AREA);
        if(component_in_vector[component_index] == false)
        {
            int i = 0;
            while(i<biggestComponents.size())
            {
                int i_area = component_stats.at<int>(biggestComponents[i], CC_STAT_AREA);
                if(component_area > i_area)
                {
                    break;
                }
                i++;
            }

            biggestComponents.insert(biggestComponents.begin() + i, component_index);

            component_in_vector[component_index] = true;
        }
    }
    }  
}
